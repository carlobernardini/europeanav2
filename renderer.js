  var Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
	var gfx = arbor.Graphics(canvas);
    var particleSystem
	var dom = $(canvas)

    var that = {
      init:function(system){
	  
        particleSystem = system

        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(80) // leave an extra 80px of whitespace per side
        
        // set up some event handlers to allow for node-dragging
        that.initMouseHandling()
		
		particleSystem.eachNode(function(node, pt){
			if (node.data.img) {	
				node.data.imageob = new Image();
				node.data.imageob.src = node.data.img;
			} else {
				node.data.imageob = false;
			}
		})
      },
      
      redraw:function(){

        ctx.fillStyle = "white"
        ctx.fillRect(0,0, canvas.width, canvas.height)
        
        particleSystem.eachEdge(function(edge, pt1, pt2){
          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords

          // draw a line from pt1 to pt2
          ctx.strokeStyle = "rgba(0,0,0, .333)"
          ctx.lineWidth = 1
          ctx.beginPath()
          ctx.moveTo(pt1.x, pt1.y)
          ctx.lineTo(pt2.x, pt2.y)
          ctx.stroke()
        })

        particleSystem.eachNode(function(node, pt){
			var w = Math.max(20, 20 + gfx.textWidth(node.data.label) )
			
			var imgob = node.data.imageob,
				imgH  = 100,
				imgW  = 100,
				offsetY = pt.y-110,
				offsetX = pt.x-50;
			if (node.data.imgSmall) {
				imgH = 40;
				imgW = 40;
				offsetY = pt.y-50;
				offsetX = pt.x-20;
			}
						
			gfx.rect(pt.x-w/2, pt.y-8, w, 20, 4, {
				fill:node.data.color,
				alpha:1.0
			})
			if (imgob) {
				ctx.drawImage(imgob, offsetX, offsetY, imgW, imgH)
			}
			gfx.text(node.data.label, pt.x, pt.y+9, {
				color: "white", 
				align: "center",
				font: "Arial",
				size:12
			})
        })    			
      },
      
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        selected = null;
        nearest = null;
        var dragged = null;
        var oldmass = 1

        var _section = null

        var handler = {
          moved:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            nearest = particleSystem.nearest(_mouseP);

            if (!nearest.node) return false

            if (nearest.node.data.shape!='dot'){
              selected = (nearest.distance < 50) ? nearest : null
              if (selected){
                 dom.addClass('linked')
              }
              else{
                 dom.removeClass('linked')
                 window.status = ''
              }
            }
            
            return false
          },
          clicked:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            nearest = dragged = particleSystem.nearest(_mouseP);
            
            if (nearest && selected && nearest.node===selected.node){
              selected.node.data['callback'](selected.node.name);
              return false
            }
            
            
            if (dragged && dragged.node !== null) dragged.node.fixed = true

            $(canvas).unbind('mousemove', handler.moved);
            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)

            return false
          },
          dragged:function(e){
            var old_nearest = nearest && nearest.node._id
            var pos = $(canvas).offset();
            var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

            if (!nearest) return
            if (dragged !== null && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){
            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null;
            // selected = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            $(canvas).bind('mousemove', handler.moved);
            _mouseP = null
            return false
          }

        }
        $(canvas).mousedown(handler.clicked);
        $(canvas).mousemove(handler.moved);
      },
    }
    return that
  }    
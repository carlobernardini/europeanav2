<?php
/* EUROPEANA PHP LIBRARY
 * Class for executing calls to the Europeana v2 API
 * @author      Carlo Bernardini <carlobernardini@gmail.com>
 * @copyright   Copyright (c) 2014, CC BY 4.0
 * @license	    <http://creativecommons.org/licenses/by-sa/4.0/>
 */
 
class Europeana {

	private $apikey;
	private $uri;
	
	/**
	 * Predefined base-URI and available endpoints according to the Europeana API-documentation
	 */
	protected $host = array(
		'base' => 'http://europeana.eu/api/v2',
		'endpoints' => array(
			'search' => '/search.json',
			'providers' => '/providers.json',
			'suggestions' => '/suggestions.json',
			'datasets' => '/provider/[id]/datasets.json',
			'record' => '/record/[id].json',
			'provider' => '/provider/[id].json',
			'dataset' => '/dataset/[id].json'
		)
	);
	
	/**
	 * Predefined facets and profiles according to the Europeana API-documentation
	 */
	protected $parameters = array(
		'facets' => array('COUNTRY', 'DATA_PROVIDER', 'LANGUAGE', 'PROVIDER', 'RIGHTS', 'TYPE', 'UGC', 'YEAR'),
		'profiles' => array('standard', 'minimal', 'facets', 'breadcrumbs', 'portal')
	);
	
	public function __construct($key){
		$this->apikey = $key;
	}
	
	/** 
	 * Function httpstatus
	 * @param  int    $code Statuscode returned by API
	 * @return string       Human readable API status
	 */
	private function httpstatus ($code) {
		$codes = array(
			200 => 'The request was executed successfully.',
			400 => 'Bad request.',
			401 => 'Authentication credentials were missing or authentication failed.',
			404 => 'The requested record was not found.',
			429 => 'The request could be served because the application has reached its usage limit.',
			500 => 'Internal server error.'
		);
		if (array_key_exists($code, $codes)) return $codes[$code];
		else return 'Status unknown';
	}
	
	/**
	 * Function fetch
	 * @param  bool   $json_output Indicates wether or not to return JSON-formatted output
	 * @param  mixed  $uri         If false $this->uri will be used, if string passed that will be used as uri
	 * @return mixed  $response    Returns associative array or json if $json_output is true
	 */
	private function fetch($json_output = false, $uri = false) {
		if (!$uri) $uri = $this->uri;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $uri);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$response = json_decode($response, true);
		if (isset($response['apikey'])) unset($response['apikey']);
		$response['_libstatus'] = array(
			'uri' => $uri,
			'code' => $http_code,
			'msg'  => $this->httpstatus($http_code)
		);
		if ($json_output) $response = json_encode($response, JSON_PRETTY_PRINT);
		return $response;
	}
	
	/**
	 * Function builduri
	 * @param  string $endpoint The desired endpoint which will be requested, must be present in $this->host['endpoints']
	 * @param  mixed  $id       If an ID is supplied it will be replaced in the request uri before returning it
	 * @return mixed  @endpoint Returns request uri or false if an error occurred when building the uri
	 */
	protected function builduri($endpoint, $id = false) {
		if (array_key_exists($endpoint, $this->host['endpoints'])) {
			$endpoint = $this->host['base'] . $this->host['endpoints'][$endpoint];
			if ($id) $endpoint = str_replace('[id]', $id, $endpoint);
			return $endpoint;
		}
		return false;
	}
	
	/**
	 * Function is_json
	 * @param  string $str The string to be verified as being JSON-formatted
	 * @return bool        Wehter or not $str is JSON-formatted
	 */
	public function is_json ($str) {
		@json_decode($str);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
	/**
	 * Function checkISO3166
	 * @param  string $code Country code to be checked with the Worldbank database
	 * @return mixed        Returns two-letter country code or false if there was no match
	 */
	public function checkISO3166($code) {
		$code = urlencode($code);
		$response = $this->fetch(false, "http://api.worldbank.org/countries/{$code}?format=json");
		if (isset($response[1])) return $response[1][0]['iso2Code'];
		else return false;
	}
	
	/**
	 * Function normalizeFacet
	 * @param  mixed  $facet Can be array array(facet, value) or facet name as string
	 * @param  string $value Facet value, only used when $facet is not an array
	 * @return mixed         Returns false if $facet is an empty array, null if facet unknown, normalized facet when successful
	 */
	public function normalizeFacet($facet, $value = false) {
		if (is_array($facet)) {
			if (empty($facet)) return false;
			$value = array_values($facet)[0];
			$facet = key($facet);
		}
		$facet = strtoupper(trim($facet));
		if (!in_array($facet, $this->parameters['facets'])) return null;
		$value = trim($value);
		return "$facet:$value";
	}
	
	/**
	 * Function search
	 * @param  array $params Mixed array of search parameters which may extend the default set of parameters
	 * @return mixed         Returns data in JSON-format or associative array depending on fetch param
	 */
	public function search($params = array()){
		$errors = array();
		if (!is_array($params)) {
			$query = urlencode($params);
		} else {
			$defaults = array(
				'wskey' => $this->apikey,
				'query' => '',
				'start' => 1,
				'rows' => 12,
				'profile' => false,
				'qf' => array(),
				'callback' => false
			);
			$remove = array_diff_key($params, $defaults);
			if (!empty($remove)) {
				foreach ($remove as $key => $v) unset($params[$key]);
			}
			$params = array_merge($defaults, $params);
			if (!empty($params['qf'])) {
				if (count($params['qf']) > 1) {
					$facets = array();
					$params['profile'] = 'facets';
					foreach ($params['qf'] as $facet => $value) {
						$facets[] = $this->normalizeFacet($facet, $value);
					}
					$params['facet'] = implode(',', $facets);
					unset($params['qf']);
				} else {
					$params['qf'] = $this->normalizeFacet($params['qf']);
				}
			}
		}
		foreach ($params as $key => $param) {
			if ($param === false) unset($params[$key]);
		}
		if (array_key_exists('profile', $params) && !in_array($params['profile'], $this->parameters['profiles'])) $params['profile'] = $this->profiles[0];
		$query = http_build_query($params, '', '&');
		$this->uri = join('?', array($this->builduri('search'), $query));
		
		return $this->fetch(true);
	}
	
	/**
	 * Function getRecord: Fetch a specific record, optionally with a list of similar items
	 * @param string      $id       The ID of the record to be fetched, required
	 * @param bool        $similar  Whether or not to request similar items as well
	 * @param bool|string $callback Which callback to be triggered, or false if none
	 * @return mixed                Returns data in JSON-format or associative array depending on fetch param
	 */
	public function getRecord($id, $similar = false, $callback = false) {
		$host = $this->builduri('record', $id);
		$callback = ($callback) ? '&callback=' . urlencode($callback) : '';
		$similar = ($similar) ? '&profile=similar' : '';
		$this->uri = "{$host}?wskey={$this->apikey}{$similar}{$callback}";
		return $this->fetch(true);
	}
	
	/**
	 * Function getDataset: Fetch information on a specific dataset
	 * @param  int   $datasetID The ID of the dataset to retrieve information about
	 * @return mixed            Returns data in JSON-format or associative array depending on fetch param
	 */
	public function getDataset($datasetID) {
		$host = $this->builduri('dataset', $datasetID);
		$this->uri = "{$host}?wskey={$this->apikey}";
		return $this->fetch(true);
	}
	
	/**
	 * Function listProviders: Fetch a list of Europeana providers, optionally filtered by country
	 * @param  int         $offset   Positive integer indicating the desired offset from beginning of results
	 * @param  int         $pageSize Positive integer indicating the desired pagesize
	 * @param  bool|string $country  If country code is passed the list of providers will be filtered on that country
	 * @return mixed                 Returns data in JSON-format or associative array depending on fetch param
	 */
	public function listProviders($offset = 1, $pageSize = 1, $country = false) {
		$params = array(
			'wskey' => $this->apikey,
			'offset' => $offset,
			'pageSize' => $pageSize
		);
		if ($country) $params['countryCode'] = $this->checkISO3166($country);
		
		$query = http_build_query($params, '', '&');
		$this->uri = join('?', array($this->builduri('providers'), $query));
		return $this->fetch(true);
	}
	
	/**
	 * Function listDatasets: Fetch a list of datasets given a specific provider ID
	 * @param  int   $providerID The ID belonging to the provider whom to list datasets from
	 * @return mixed             Returns data in JSON-format or associative array depending on fetch param
	 */
	public function listDatasets($providerID) {
		if (!is_numeric($providerID)) return "No provider id given.";
		$this->uri = $this->builduri('datasets', $providerID) . "?wskey={$this->apikey}";
		return $this->fetch(true);
	}
	
	/**
	 * Function getSuggestions: Fetch a list of suggested terms for a provided query input
	 * @param  string      $query    The query input to fetch suggestions for
	 * @param  int         $rows     A positive integer indication the desired maximum number of results to be returned, defaults to 10
	 * @param  bool|string $callback Wether or not to pass a callback function
	 * @return mixed                 Returns data in JSON-format or associative array depending on fetch param
	 */
	public function getSuggestions($query, $rows = 10, $callback = false) {
		$params = array(
			'wskey' => $this->apikey,
			'query' => strtolower($query),
			'rows' => (is_numeric($rows)) ? $rows : 10,
			'callback' => ($callback) ? $callback : ''
		);
		$query = http_build_query($params, '&');
		$this->uri = join('?', array($this->builduri('suggestions'), $query));
		return $this->fetch(true);
	}
	
	/**
	 * Function fetchExtSource: Public function for fetching data from some other source than Europeana
	 */
	public function fetchExtSource ($uri) {
		return $this->fetch(true, $uri);
	}
	
}
?>
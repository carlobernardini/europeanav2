<?php
function request ($url) {
	if (!function_exists('curl_init')) {
		die ('CURL not available');
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

$query = '
		PREFIX edm: <http://www.europeana.eu/schemas/edm/>
		PREFIX ore: <http://www.openarchives.org/ore/terms/>

		SELECT DISTINCT ?CHO ?year
		WHERE {

				?EuropeanaObject  edm:year  ?year ;
								  edm:hasMet <http://sws.geonames.org/3017382/> . 
				?EuropeanaObject ore:proxyFor ?CHO.
				FILTER (?year < "1800") 
				FILTER (?year > "1700")

		}
		ORDER BY asc (?year) LIMIT 5
		';

$query = "
		PREFIX dc: <http://purl.org/dc/elements/1.1/>
		SELECT ?node ?title ?description ?creator
			WHERE{
				?node dc:title ?title .
				?node dc:description ?description.
				?node dc:creator ?creator.
				FILTER regex(?creator, 'gogh', 'i')
			}
		LIMIT 5
";

$query = '
PREFIX edm: <http://www.europeana.eu/schemas/edm/>
PREFIX ore: <http://www.openarchives.org/ore/terms/>
PREFIX dc: <http://purl.org/dc/elements/1.1/> 

SELECT ?title ?similar ?mediaURL ?creator ?source 
WHERE {
	?resource 	dc:creator "Leonardo da Vinci";
				ore:proxyIn ?proxy ;
				dc:title ?title ;
				dc:source ?source.
	?proxy	edm:isShownBy ?mediaURL;
			edm:isRelatedTo ?similar.
}
LIMIT 100';
$request = 'http://europeana.ontotext.com/sparql.json?query=' . urlencode($query);
$response = request($request);

header("Content-type: application/json");
print ($response);
?>